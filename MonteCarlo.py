import datetime
import random
from random import choice
from math import log, sqrt

#Game logic for Ultimate TicTacToe.
class Board:    
    def __init__(self):
        self.state = []                                                        #Stores game state.
        self.legal_moves = []                                                  #Stores a list of legal plays from the current game state.
        self.win_history = []                                                  #Keep track of winners.
        self.game = [' '] * 10                                                 #Big Squares 3x3. Keeps track of major square winnings.
        self.board = [(index, ' ') for index in range (1, 82)]                                              #Small Squares 9x9.
        self.first = True                                                      #Is it the first move?
        self.free_move = False                                                 #Used when the section directed to cannot be played in.
        
        self.position = ([1, 4, 7, 28, 31, 34, 55, 58, 61],                    #Squares pointing to upper left (game[1])
                         [2, 5, 8, 29, 32, 35, 56, 59, 62],                    #Squares pointing to upper middle (game[2])
                         [3, 6, 9, 30, 33, 36, 57, 60, 63],                    #Squares pointing to upper right (game[3])
                         [10, 13, 16, 37, 40, 43, 64, 67, 70],                 #Squares pointing to center left (game[4])
                         [11, 14, 17, 38, 41, 44, 65, 68, 71],                 #Squares pointing to center middle (game[5])
                         [12, 15, 16, 39, 42, 45, 66, 69, 72],                 #Squares pointing to center right (game[6])
                         [19, 22, 25, 46, 49, 52, 73, 76, 79],                 #Squares pointing to lower left (game[7])
                         [20, 23, 26, 47, 50, 53, 74, 77, 80],                 #Squares pointing to lower middle (game[8])
                         [21, 24, 27, 48, 51, 54, 75, 78, 81])                 #Squares pointing to lower right (game[9])
        
        self.section = ([1, 2, 3, 10, 11, 12, 19, 20, 21],                     #game[1]
                        [4, 5, 6, 13, 14, 15, 22, 23, 24],                     #game[2]
                        [7, 8, 9, 16, 17, 18, 25, 26, 27],                     #game[3]
                        [28, 29, 30, 37, 38, 39, 46, 47, 48],                  #game[4]
                        [31, 32, 33, 40, 41, 42, 49, 50, 51],                  #game[5]
                        [34, 35, 36, 43, 44, 45, 52, 53, 54],                  #game[6]
                        [55, 56, 57, 64, 65, 66, 73, 74, 75],                  #game[7]
                        [58, 59, 60, 67, 68, 69, 76, 77, 78],                  #game[8]
                        [61, 62, 63, 70, 71, 72, 79, 80, 81])                  #game[9]
        
    #Display the board in its current state.
    def draw_board(self, square):
        print('\n-------------------------------------\n' +
              '| ' + str(square[1]) + ' | ' + str(square[2]) + ' | ' + str(square[3]) + ' | ' +  str(square[4]) + ' | ' + str(square[5]) + ' | ' + str(square[6]) + ' | ' + str(square[7]) + ' | ' + str(square[8]) + ' | ' + str(square[9]) + ' |' +
              '\n-------------------------------------\n' +
              '| ' + str(square[10]) + ' | ' + str(square[11]) + ' | ' + str(square[12]) + ' | ' + str(square[13]) + ' | ' + str(square[14]) + ' | ' + str(square[15]) + ' | ' + str(square[16]) + ' | ' + str(square[17]) + ' | ' + str(square[18]) + ' |' +
              '\n-------------------------------------\n' +
              '| ' + str(square[19]) + ' | ' + str(square[20]) + ' | ' + str(square[21]) + ' | ' + str(square[22]) + ' | ' + str(square[23]) + ' | ' + str(square[24]) + ' | ' + str(square[25]) + ' | ' + str(square[26]) + ' | ' + str(square[27]) + ' |' +
              '\n-------------------------------------\n' +
              '| ' + str(square[28]) + ' | ' + str(square[29]) + ' | ' + str(square[30]) + ' | ' + str(square[31]) + ' | ' + str(square[32]) + ' | ' + str(square[33]) + ' | ' + str(square[34]) + ' | ' + str(square[35]) + ' | ' + str(square[36]) + ' |' +
              '\n-------------------------------------\n' +
              '| ' + str(square[37]) + ' | ' + str(square[38]) + ' | ' + str(square[39]) + ' | ' + str(square[40]) + ' | ' + str(square[41]) + ' | ' + str(square[42]) + ' | ' + str(square[43]) + ' | ' + str(square[44]) + ' | ' + str(square[45]) + ' |' +
              '\n-------------------------------------\n' +
              '| ' + str(square[46]) + ' | ' + str(square[47]) + ' | ' + str(square[48]) + ' | ' + str(square[49]) + ' | ' + str(square[50]) + ' | ' + str(square[51]) + ' | ' + str(square[52]) + ' | ' + str(square[53]) + ' | ' + str(square[54]) + ' |' +
              '\n-------------------------------------\n' +
              '| ' + str(square[55]) + ' | ' + str(square[56]) + ' | ' + str(square[57]) + ' | ' + str(square[58]) + ' | ' + str(square[59]) + ' | ' + str(square[60]) + ' | ' + str(square[61]) + ' | ' + str(square[62]) + ' | ' + str(square[63]) + ' |' +
              '\n-------------------------------------\n' +
              '| ' + str(square[64]) + ' | ' + str(square[65]) + ' | ' + str(square[66]) + ' | ' + str(square[67]) + ' | ' + str(square[68]) + ' | ' + str(square[69]) + ' | ' + str(square[70]) + ' | ' + str(square[71]) + ' | ' + str(square[72]) + ' |' +
              '\n-------------------------------------\n' +
              '| ' + str(square[73]) + ' | ' + str(square[74]) + ' | ' + str(square[75]) + ' | ' + str(square[76]) + ' | ' + str(square[77]) + ' | ' + str(square[78]) + ' | ' + str(square[79]) + ' | ' + str(square[80]) + ' | ' + str(square[81]) + ' |' +
              '\n-------------------------------------\n')
    
    #Get record of winners. Do this once all games have been played.
    def get_win_history(self):
        return self.win_history
    
    #True/False: Can the move suggested be played?    
    def is_legal(self, move, board):
        #Move freely under 2 circumstances:
        #1) The first move of the game.
        #2) The 3x3 to be played in has been filled completely.
        if self.first == True or self.free_move == True:            
            #If the space is not claimed, take it.
            if board[move] == ' ':
                return True
            if board[move] != ' ':
                return False
        
        last_move, player = self.state

        #Check if the currently suggested move is within the 3x3 it must be played in.
        if last_move in self.position[0] and move in self.section[0]:
            if ' ' not in list((board[1], board[2], board[3], board[10], board[11], board[12], board[19], board[20], board[21])):
                #If the correct section to be played in is filled, allow the player to freely select another move.
                self.free_move = True
                #Treat as illegal. Select new move freely.
                return False
                                       
            #Legal if square is not taken.
            if board[move] == ' ':
                return True
            
        if last_move in self.position[1] and move in self.section[1]:
            if ' ' not in list((board[4], board[5], board[6], board[13], board[14], board[15], board[22], board[23], board[24])):
                self.free_move = True
                return False

            if board[move] == ' ':
                return True
        
        if last_move in self.position[2] and move in self.section[2]:
            if ' ' not in list((board[7], board[8], board[9], board[16], board[17], board[18], board[25], board[26], board[27])):
                self.free_move = True
                return False

            if board[move] == ' ':
                return True
            
        if last_move in self.position[3] and move in self.section[3]:
            if ' ' not in list((board[28], board[29], board[30], board[37], board[38], board[39], board[46], board[47], board[48])):
                self.free_move = True
                return False

            if board[move] == ' ':
                return True
            
        if last_move in self.position[4] and move in self.section[4]:
            if ' ' not in list((board[31], board[32], board[33], board[40], board[41], board[42], board[49], board[50], board[51])):
                self.free_move = True
                return False

            if board[move] == ' ':
                return True
            
        if last_move in self.position[5] and move in self.section[5]: 
            if ' ' not in list((board[34], board[35], board[36], board[43], board[44], board[45], board[52], board[53], board[54])):
                self.free_move = True
                return False

            if board[move] == ' ':
                return True
            
        if last_move in self.position[6] and move in self.section[6]:
            if ' ' not in list((board[55], board[56], board[57], board[64], board[65], board[66], board[73], board[74], board[75])):
                self.free_move = True
                return False

            if board[move] == ' ':
                return True
            
        if last_move in self.position[7] and move in self.section[7]:
            if ' ' not in list((board[58], board[59], board[60], board[67], board[68], board[69], board[76], board[77], board[78])):
                self.free_move = True
                return False

            if board[move] == ' ':
                return True
            
        if last_move in self.position[8] and move in self.section[8]:
            if ' ' not in list((board[61], board[62], board[63], board[70], board[71], board[72], board[79], board[80], board[81])):
                self.free_move = True
                return False

            if board[move] == ' ':
                return True
        #Illegal move suggested.
        return False
    
    #Return a list of possible moves from the current game state.
    def get_legal_moves(self, move, board):        
        if self.first == True:
            self.legal_moves = [x for x, value in board]

        #Check for the section the last move was made in.
        if move in self.section[0]:
            #Check if the 3x3 section has playable spaces.
            if ' ' not in list((board[1], board[2], board[3], board[10], board[11], board[12], board[19], board[20], board[21])):
                #Free move if no possible play in the current section.
                self.legal_moves = [x for x, value in board if value == ' ']

            else:
                #Append the unoccupied spaces in the section.
                y = (board[1], board[2], board[3], board[10], board[11], board[12], board[19], board[20], board[21])
                self.legal_moves = [x for x, value in y if value == ' ']
            
        if move in self.section[1]:
            if ' ' not in list((board[4], board[5], board[6], board[13], board[14], board[15], board[22], board[23], board[24])):
                self.legal_moves = [x for x, value in board if value == ' ']
                
            else:
                y = (board[4], board[5], board[6], board[13], board[14], board[15], board[22], board[23], board[24])
                self.legal_moves = [x for x, value in y if value == ' ']

        if move in self.section[2]:
            if ' ' not in list((board[7], board[8], board[9], board[16], board[17], board[18], board[25], board[26], board[27])):
                self.legal_moves = [x for x, value in board if value == ' ']
                
            else:
                y = (board[7], board[8], board[9], board[16], board[17], board[18], board[25], board[26], board[27])
                self.legal_moves = [x for x, value in y if value == ' ']
            
        if move in self.section[3]:
            if ' ' not in list((board[28], board[29], board[30], board[37], board[38], board[39], board[46], board[47], board[48])):
                self.legal_moves = [x for x, value in board if value == ' ']
                
            else:
                y = (board[28], board[29], board[30], board[37], board[38], board[39], board[46], board[47], board[48])
                self.legal_moves = [x for x, value in y if value == ' ']
                
        if move in self.section[4]:
            if ' ' not in list((board[31], board[32], board[33], board[40], board[41], board[42], board[49], board[50], board[51])):
                self.legal_moves = [x for x, value in board if value == ' ']
                
            else:
                y = (board[31], board[32], board[33], board[40], board[41], board[42], board[49], board[50], board[51])
                self.legal_moves = [x for x, value in y if value == ' ']
            
        if move in self.section[5]: 
            if ' ' not in list((board[34], board[35], board[36], board[43], board[44], board[45], board[52], board[53], board[54])):
                self.legal_moves = [x for x, value in board if value == ' ']
                
            else:
                y = (board[34], board[35], board[36], board[43], board[44], board[45], board[52], board[53], board[54])
                self.legal_moves = [x for x, value in y if value == ' ']
            
        if move in self.section[6]:
            if ' ' not in list((board[55], board[56], board[57], board[64], board[65], board[66], board[73], board[74], board[75])):
                self.legal_moves = [x for x, value in board if value == ' ']
                
            else:
                y = (board[55], board[56], board[57], board[64], board[65], board[66], board[73], board[74], board[75])
                self.legal_moves = [x for x, value in y if value == ' ']

        if move in self.section[7]:
            if ' ' not in list((board[58], board[59], board[60], board[67], board[68], board[69], board[76], board[77], board[78])):
                self.legal_moves = [x for x, value in board if value == ' ']
                
            else:
                y = (board[58], board[59], board[60], board[67], board[68], board[69], board[76], board[77], board[78])
                self.legal_moves = [x for x, value in y if value == ' ']
            
        if move in self.section[8]:
            if ' ' not in list((board[61], board[62], board[63], board[70], board[71], board[72], board[79], board[80], board[81])):
                self.legal_moves = [x for x, value in board if value == ' ']
                
            else:
                y = (board[61], board[62], board[63], board[70], board[71], board[72], board[79], board[80], board[81])
                self.legal_moves = [x for x, value in y if value == ' ']
        #Return the list.
        return self.legal_moves
    
    #Update values after each turn.        
    def make_move(self, move, game):
        move, player = move
        
        if self.is_legal(move, self.board):
            self.board[move] = player
            
            if self.free_move == True:
                self.free_move = False
                
            if self.first == True:                                                   
                self.first = False
                self.state = [move, player]
                
            else:                
                self.state.append([move, player])
                           
            self.board, self.game = self.update_3x3(move, player, self.board, game)
            self.draw_board(self.board) 
            
    #Reset the game with initial values. Do not erase win history.
    def reset_board(self):
        self.state = []
        self.game = [' '] * 10
        self.board = [' '] * 82
        self.first = True
        self.free_move = True
        self.legal_moves = []
        
    #Check if the square last played in has been won by the player.
    def update_3x3(self, move, player, board, game):   
        #Check for which 3x3 the last move was made in. Proceed to evaluate for a winner in that square.
        if move in self.section[0]:
            if ((board[1] == player and board[2] == player and board[3] == player) or           #top
                (board[10] == player and board[11] == player and board[12] == player) or        #middle
                (board[19] == player and board[20] == player and board[21] == player) or        #bottom
                (board[1] == player and board[10] == player and board[19] == player) or         #left
                (board[2] == player and board[11] == player and board[20] == player) or         #center
                (board[3] == player and board[12] == player and board[21] == player) or         #right
                (board[1] == player and board[11] == player and board[21] == player) or         #diagonal
                (board[3] == player and board[11] == player and board[19] == player)):          #diagonal 2
                #If the player wins a square, prevent further inputs to it by assigning all values to the player's.
                board[1] = player
                board[2] = player
                board[3] = player
                board[10] = player
                board[11] = player
                board[12] = player
                board[19] = player
                board[20] = player
                board[21] = player
                #Fill in the square in the 3x3 corresponding to the one the player won in the overall game.
                game[1] = player
                
            #Check if all squares have been filled in (tie/ no winner). Each square in the 3x3 will be assigned 0 in the case of a tie (Opposed to 1 || 2).
            elif ' ' not in list((board[1], board[2], board[3], board[10], board[11], board[12], board[19], board[20], board[21])):
                game[1] = '0'
                
        if move in self.section[1]:
            if ((board[4] == player and board[5] == player and board[6] == player) or           #top
               (board[13] == player and board[14] == player and board[15] == player) or         #middle
               (board[22] == player and board[23] == player and board[24] == player) or         #bottom
               (board[4] == player and board[13] == player and board[22] == player) or          #left
               (board[5] == player and board[14] == player and board[23] == player) or          #center
               (board[6] == player and board[15] == player and board[24] == player) or          #right
               (board[4] == player and board[14] == player and board[24] == player) or          #diagonal
               (board[6] == player and board[14] == player and board[22] == player)):           #diagonal 2
                board[4] = player
                board[5] = player
                board[6] = player
                board[13] = player
                board[14] = player
                board[15] = player
                board[22] = player
                board[23] = player
                board[24] = player
                game[2] = player
                
            elif ' ' not in list((board[4], board[5], board[6], board[13], board[14], board[15], board[22], board[23], board[24])):
                game[2] = '0'
                
        if move in self.section[2]:
            if ((board[7] == player and board[8] == player and board[9] == player) or           #top
                (board[16] == player and board[17] == player and board[18] == player) or        #middle
                (board[25] == player and board[26] == player and board[27] == player) or        #bottom
                (board[7] == player and board[16] == player and board[25] == player) or         #left
                (board[8] == player and board[17] == player and board[26] == player) or         #center
                (board[9] == player and board[18] == player and board[27] == player) or         #right
                (board[7] == player and board[17] == player and board[27] == player) or         #diagonal
                (board[9] == player and board[17] == player and board[25] == player)):          #diagonal 2
                board[7] = player
                board[8] = player
                board[9] = player
                board[16] = player
                board[17] = player
                board[18] = player
                board[25] = player
                board[26] = player
                board[27] = player
                game[3] = player
                
            elif ' ' not in list((board[7], board[8], board[9], board[16], board[17], board[18], board[25], board[26], board[27])):
                game[3] = '0'
                
        if move in self.section[3]:
            if ((board[28] == player and board[29] == player and board[30] == player) or        #top
                (board[37] == player and board[38] == player and board[39] == player) or        #middle
                (board[46] == player and board[47] == player and board[48] == player) or        #bottom
                (board[28] == player and board[37] == player and board[46] == player) or        #left
                (board[29] == player and board[38] == player and board[47] == player) or        #center
                (board[30] == player and board[39] == player and board[48] == player) or        #right
                (board[28] == player and board[38] == player and board[48] == player) or        #diagonal
                (board[30] == player and board[38] == player and board[46] == player)):         #diagonal 2
                board[28] = player
                board[29] = player
                board[30] = player
                board[37] = player
                board[38] = player
                board[39] = player
                board[46] = player
                board[47] = player
                board[48] = player
                game[4] = player
                
            elif ' ' not in list((board[28], board[29], board[30], board[37], board[38], board[39], board[46], board[47], board[48])):
                game[4] = '0'
                
        if move in self.section[4]:
            if ((board[31] == player and board[32] == player and board[33] == player) or        #top
                (board[40] == player and board[41] == player and board[42] == player) or        #middle
                (board[49] == player and board[50] == player and board[51] == player) or        #bottom
                (board[31] == player and board[40] == player and board[49] == player) or        #left
                (board[32] == player and board[41] == player and board[50] == player) or        #center
                (board[33] == player and board[42] == player and board[51] == player) or        #right
                (board[31] == player and board[41] == player and board[51] == player) or        #diagonal
                (board[33] == player and board[41] == player and board[49] == player)):         #diagonal 2
                board[31] = player
                board[32] = player
                board[33] = player
                board[40] = player
                board[41] = player
                board[42] = player
                board[49] = player
                board[50] = player
                board[51] = player
                game[5] = player
                
            elif ' ' not in list((board[31], board[32], board[33], board[40], board[41], board[42], board[49], board[50], board[51])):
                game[5] = '0'
                
        if move in self.section[5]:
            if ((board[34] == player and board[35] == player and board[36] == player) or        #top
                (board[43] == player and board[44] == player and board[45] == player) or        #middle
                (board[52] == player and board[53] == player and board[54] == player) or        #bottom
                (board[34] == player and board[43] == player and board[52] == player) or        #left
                (board[35] == player and board[44] == player and board[53] == player) or        #center
                (board[36] == player and board[45] == player and board[54] == player) or        #right
                (board[34] == player and board[44] == player and board[54] == player) or        #diagonal
                (board[36] == player and board[44] == player and board[52] == player)):         #diagonal 2
                board[34] = player
                board[35] = player
                board[36] = player
                board[43] = player
                board[44] = player
                board[45] = player
                board[52] = player
                board[53] = player
                board[54] = player
                game[6] = player
                
            elif ' ' not in list((board[34], board[35], board[36],  board[43], board[44], board[45], board[52], board[53], board[54])):
                game[6] = '0'
                
        if move in self.section[6]:
            if ((board[55] == player and board[56] == player and board[57] == player) or        #top
                (board[64] == player and board[65] == player and board[66] == player) or        #middle
                (board[73] == player and board[74] == player and board[75] == player) or        #bottom
                (board[55] == player and board[64] == player and board[73] == player) or        #left
                (board[56] == player and board[65] == player and board[74] == player) or        #center
                (board[57] == player and board[66] == player and board[75] == player) or        #right
                (board[55] == player and board[65] == player and board[75] == player) or        #diagonal
                (board[57] == player and board[65] == player and board[73] == player)):         #diagonal 2
                board[55] = player
                board[56] = player
                board[57] = player
                board[64] = player
                board[65] = player
                board[66] = player
                board[73] = player
                board[74] = player
                board[75] = player
                game[7] = player
                
            elif ' ' not in list((board[55], board[56], board[57], board[64], board[65], board[66], board[73], board[74], board[75])):
                game[7] = '0'
                
        if move in self.section[7]:
            if ((board[58] == player and board[59] == player and board[60] == player) or        #top
                (board[67] == player and board[68] == player and board[69] == player) or        #middle
                (board[76] == player and board[77] == player and board[78] == player) or        #bottom
                (board[58] == player and board[67] == player and board[76] == player) or        #left
                (board[59] == player and board[68] == player and board[77] == player) or        #center
                (board[60] == player and board[69] == player and board[78] == player) or        #right
                (board[58] == player and board[68] == player and board[78] == player) or        #diagonal
                (board[60] == player and board[68] == player and board[76] == player)):         #diagonal 2
                board[58] = player
                board[59] = player
                board[60] = player
                board[67] = player
                board[68] = player
                board[69] = player
                board[76] = player
                board[77] = player
                board[78] = player
                game[8] = player
                
            elif ' ' not in list((board[58], board[59], board[60], board[67], board[68], board[69], board[76], board[77], board[78])):
                game[8] = '0'
                
        if move in self.section[8]:
            if ((board[61] == player and board[62] == player and board[63] == player) or        #top
                (board[70] == player and board[71] == player and board[72] == player) or        #middle
                (board[79] == player and board[80] == player and board[81] == player) or        #bottom
                (board[61] == player and board[70] == player and board[79] == player) or        #left
                (board[62] == player and board[71] == player and board[80] == player) or        #center
                (board[63] == player and board[72] == player and board[81] == player) or        #right
                (board[61] == player and board[71] == player and board[81] == player) or        #diagonal
                (board[63] == player and board[71] == player and board[79] == player)):         #diagonal 2
                board[61] = player
                board[62] = player
                board[63] = player
                board[70] = player
                board[71] = player
                board[72] = player
                board[79] = player
                board[80] = player
                board[81] = player
                game[9] = player
                
            elif ' ' not in list((board[61], board[62], board[63], board[70], board[71], board[72], board[79], board[80], board[81])):
                game[9] = '0'
        
        return (board, game)
        
    #Determine if the game is over.
    def game_status(self, player, game):
        #Check if player has secured 3 specific squares in a line first.
        if ((game[1] == player and game[2] == player and game[3] == player) or #top
            (game[3] == player and game[4] == player and game[5] == player) or #middle
            (game[7] == player and game[8] == player and game[9] == player) or #bottom
            (game[1] == player and game[4] == player and game[7] == player) or #left
            (game[2] == player and game[5] == player and game[8] == player) or #center
            (game[3] == player and game[6] == player and game[9] == player) or #right
            (game[1] == player and game[5] == player and game[9] == player) or #diagonal
            (game[3] == player and game[5] == player and game[7] == player)):  #diagonal 2
            return 1      
        #Check if game is still playable.
        if ' ' not in game:
            return 2       
        #Game is still playable.
        return 3

#MCTS AI using UCT algorithm
class MonteCarlo:
    def __init__(self, board, player_num, c = 1.4, t = 30, m = 30):
        self.board = board                                                     #Instance of the game.
        self.player = player_num
        self.turn_time = datetime.timedelta(seconds = t)                       #How long the AI has to calculate moves.
        self.max_moves = m                                                     #The maximum number of moves the AI can look ahead.
        self.wins = {}                                                         #Dictionary for number of wins logged in branch.
        self.moves = {}                                                        #Dictionary for the branches visited in simulation.
        self.c = c                                                             #Higher values of c = more exploration. Lower values of c = more exploitation.
        self.id = "MonteCarlo " + str(player_num)                              #For the purposes of collecting a win_history.
    
    #Calculate and propose the best possible move from the current game state.
    def get_move(self):
        if self.board.first == True:
            self.board.state = [random.randint(1, 82), self.player]

        current_state = self.board.state
        player = self.player
        legal_moves = self.board.get_legal_moves(current_state, self.board.board)
        
        #If there are no legal moves, exit.
        if not legal_moves:
            return
        
        #If there's only one possible move, make it.
        if len(legal_moves) == 1:
            return legal_moves
        
        start_time = datetime.datetime.utcnow()
        
        #Limit the AI's time to calculate its move to __ seconds starting from when function was called.
        while (datetime.datetime.utcnow() - start_time) < self.turn_time:
            self.run_simulation(current_state)

        #Selection (Select the move with the most wins)
        states = [(move, current_state) for move in legal_moves]
        n_win, move = max((self.wins.get((move, player), 0) / self.moves.get((move, player), 1), state) for (move, state) in states)
        return move
        
    #Simulates a game using either UCB || random selection from the current game state.
    #Utilize copied values to avoid interference with actual game state.
    def run_simulation(self, state):
        states_visited = set()
        current_state = state
        player = self.player
        board = self.board.board
        game = self.board.game
        expand = True

        for i in range(0, self.max_moves):
            legal_moves = self.board.get_legal_moves(current_state, board)

            #Use UCT to select a move. UCT = (xi/ni) + c(sqrt(ln * n / ni))
            if all(self.moves.get((move, player)) for (move) in legal_moves):
                total_log = log(sum(self.moves[(state, player)] for (state) in legal_moves))
                value, move = max(((self.wins[(move, player)] / self.moves[(move, player)]) + self.c * sqrt(total_log / self.moves[(move, player)]), 
                                          move) for (move) in legal_moves)

            #Select a random legal move.
            else:
                move = choice(legal_moves)
            
            if expand and (move, player) not in self.moves:
                expand = False
                self.moves[(move, player)] = 0
                self.wins[(move, player)] = 0              
              
            states_visited.add((move, player))
            board, game = self.board.update_3x3(current_state, player, board, game)
            
            if self.board.game_status(player, game) == 1 or self.board.game_status(player, game) == 2:
                break
        
        for (move, player) in states_visited:
            if (move, player) not in self.moves:
                continue
            
            self.moves[(move, player)] += 1       
            status = self.board.game_status(player, game)
            
            if status == 1 and player == self.player:
                self.wins[(move, player)] += 1


class TicTacToe:
    def __init__(self, reps):
        self.game = Board()
        self.player_1 = MonteCarlo(self.game, 1, 1.2, 60, 15)
        self.player_2 = MonteCarlo(self.game, 2, 1.8, 60, 15)
        self.player_3 = MonteCarlo(self.game, 1, 1.8, 90, 15)
        self.player_4 = MonteCarlo(self.game, 2, 1.8, 90, 30)
        self.player_5 = MonteCarlo(self.game, 1, 0.6, 120, 50)
        self.player_6 = MonteCarlo(self.game, 2, 2.2, 120, 50)
        self.games_left = reps
        
    #For Human vs AI or Human vs Human
    def play(self):
        pass
    
    #AI vs AI
    def simulate(self, player_1, player_2):
        players = (player_1, player_2)
        player_to_move = choice(players)
        
        while self.games_left > 0:
            while self.game.game_status(player_1, self.game.game) == 3 and self.game.game_status(player_2, self.game.game) == 3:
                if player_to_move == player_1:
                    print("Player 1's Turn.")
                    move = player_1.get_move()                        
                    print("Move chosen is " + str(move))
                    self.game.make_move(move, self.game.game)
                    player_to_move = player_2
                    
                if player_to_move == player_2:
                    print("Player 2's Turn.")
                    move = player_2.get_move()                              
                    print("Move chosen is " + str(move))
                    self.game.make_move(move, self.game.game)
                    player_to_move = player_1

            if self.game.game_status(player_1, self.game.game) == 1:
                self.game.win_history.append(player_1.id)
                print(player_1.id + " wins!")
            
            elif self.game.game_status(player_2, self.game.game) == 1:
                self.game.win_history.append(player_2.id)
                print(player_2.id + " wins!")
            
            else:
                self.game.win_history.append("Tied.")
                print("The match resulted in a draw.")
            self.game.reset_board()    
            self.games_left -= 1
                
game = TicTacToe(1)
game.simulate(game.player_1, game.player_2)
print(game.game.win_history)